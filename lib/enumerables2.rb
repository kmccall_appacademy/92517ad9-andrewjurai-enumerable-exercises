require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  return 0 if arr.empty?
  arr.reduce(:+)
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? { |mainstring| mainstring.include? substring }
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  chars = string.split.map(&:chars).flatten
  repeat_letters = Array.new
  chars.each { |ch| repeat_letters << ch if repeated_letter?(ch, chars) }
  repeat_letters.uniq.sort
end

def repeated_letter?(letter, array_of_letters)
  occurances = array_of_letters.count letter
  occurances > 1
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  sorted_by_length = string.split.sort_by(&:length).reverse
  sorted_by_length[0..1]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  ("a".."z").select { |ch| ch unless string.include? ch }
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).select { |year| year unless repeated_digits?(year) }
end

def repeated_digits?(year)
  amt_uniq_digits = year.to_s.chars.uniq.count
  total_digits = year.to_s.chars.count
  amt_uniq_digits != total_digits
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?
def one_week_wonders(songs)
  uniq_songs = songs.uniq
  uniq_songs.select { |song| song unless songs.join.include?(song + song) }
end


# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  string = remove_punctuation(string).split
  string.select! { |word| word if word.include?("c") }
  string.sort_by { |word| c_distance(word) }.first
end

def c_distance(word)
  word.reverse.index("c")
end

def remove_punctuation(word)
  word.delete(".,;/?!")
end
# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  repeat_arr = Array.new
  counter = 0
  arr.each.with_index do |int, i|
    next if int == arr[i - 1] && i > 0

    if int == arr[i + 1]
      counter = i
      while int == arr[counter + 1]
        counter += 1
      end
      repeat_arr << [i, counter]
    end
  end
  repeat_arr
end
